
# **Ejercicio 2. Crear un StatefulSet con 3 instancias de MongoDB**

### **STATEFULSET**
El StatefulSet usado se declara a continuación ***(ej3-ss.yml)***:
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo
spec:
  selector:
    matchLabels:
      app: db
      name: mongodb
  serviceName: "mongo"
  replicas: 3
  template:
    metadata:
      labels:
        app: db
        name: mongodb
    spec:
      # terminationGracePeriodSeconds: 10
      containers:
      - name: mongo
        image: mongo:4.4.1
        command:
          - mongod
          - "--replSet"
          - rs0
          - "--dbpath=/data/db"
          - "--bind_ip"
          - "0.0.0.0"
        ports:
          - containerPort: 27017
        volumeMounts:
          - name: mongo-persistent-storage
            mountPath: /data/db
  volumeClaimTemplates:
    - metadata:
        name: mongo-persistent-storage
      spec:
        accessModes: ["ReadWriteOnce"]
        resources:
          requests:
            storage: 5Gi
```
Se lanza dicho fichero haciendo el siguiente comando:
```
kubectl appply -f ej3-ss.yml
```
<br><br/>

---
### **ENTRADA AL POD**
A continuación se debe acceder a uno de los pods haciendo el siguiente comando:
```
kubectl exec -ti --tty mongo-0 bin/bash 
```
Una vez dentro del pod, se deberá acceder al intérprete de mongo escribiendo "mongo" en la consola.

<br><br/>

---
### **CREACIÓN CLUSTER**
Una vez dentro, se procede a crear el clúster con el siguiente comando:
```
rs.initiate( {
   _id : "rs0",
   members: [
      { _id: 0, host: "172.17.0.6:27017" },
      { _id: 1, host: "172.17.0.7:27017" },
      { _id: 2, host: "172.17.0.8:27017" }
   ]
})
```
Es importante tener en cuenta que el pod al cual se ha realizado la conexión, sea el de id=0. Esto es debido a que ese será tratado como *PRIMARY*, y es sobre el cuál se ejercerán las distintas operaciones del ejercicio.
Para comprobar que esto funciona bien hasta ahora, se puede ejecutar el comando:
```
rs.status()
```
Este comando, da información del clúster creado, y permitirá visualizar el state a primary del pod con índice 0.

<br><br/>

---
### **CREACIÓN USUARIO**
A continuación, se creará un usuario en esa instancia del clúster. Si todo funciona correctamente, se podrá acceder a otro pod distinto con el mismo usuario creado en el pod tratado como *PRIMARY*.
```
use test_db
db.createUser(
  {
    user: "miqueljorda",
    pwd: "1234",
    roles: [
       { role: "readWrite", db: "test_db" }  
    ]
  }
)
```
<br><br/>

---
### **COMPROBACIÓN**
Salir del del cluster y del pod *(haciendo exit)* para conectarse a otro pod del mismo cluster:

```
kubectl exec -it --tty mongo-1 bin/bash
```

probar de conectarse a la instancia de mongo con el usuario creado en el pod PRIMARY:
```
mongo --username miqueljorda --password 1234 --authenticationDatabase test_db
```
<br><br/>

---
### **DIFERENCIAS ENTRE REPLICASET Y STATEFULSET**
StatefulSet comparte las propiedades del nodo primario entre los demás. Esta característica es la diferencial respecto ReplicaSet.
