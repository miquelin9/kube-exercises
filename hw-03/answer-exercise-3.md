# **Ejercicio 3. Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.**

Primero de todo, se debera habilitar el siguiene addon ejecutando el comando:
```
minikube addons enable metrics-server
```
<br><br/>

---
### **DEPLOYMENT**
Posteriormente, se deberá crear un Deployment ***(ej3-depl.yml)***

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  strategy: 
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v1.0.0
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            memory: "256Mi"
            cpu: 100m
          requests:
            memory: "256Mi"
            cpu: 100m
        ports:
        - containerPort: 80
```
Una vez creado, se lanza con el comando:

```
kubectl apply -f ej3-depl.yml
```
<br><br/>

---
### **SERVICE**
A continuación, hay que crear el servicio ***(ej3-svc.yml)***:

```
apiVersion: v1
kind: Service
metadata:
  name: service1
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```
Se lanza ejecutando el comando:

```
kubectl apply -f ej3-svc.yml
```
<br><br/>

---
### **HORIZONTAL POD AUTOSCALER**
Seguidamente se requiere el objeto de tipo HorizontalPodAutoscaler, definido a continuación ***(ej3-hs.yml)***:

```
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ej3-hs
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-deployment
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 50
```
Se usará de nuevo el mismo comando para lanzar este objeto:
```
kubectl apply -f ej3-hs.yml
```

<br><br/>

---
### **COMPROVACIÓN**
Una vez esté todo corriendo, accederemos a la url del servicio para poder, posteriormente, ejecutar un comando de estrés y corroborar que el HorizontalPodAutoscaler funciona.

```
minikube service service1 --url para obtener la url del servicio
```
Comando para aplicar estrés:

```
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://192.168.99.100:30259; done"
```
<br><br/>

---
### **IMÁGENES DEL PROCESO**
![](./resources/images/captura-hpa0.PNG)
![](./resources/images/captura-hpa1.PNG)
![](./resources/images/captura-hpa2.PNG)
![](./resources/images/captura-hpa3.PNG)
![](./resources/images/captura-hpa4.PNG)
