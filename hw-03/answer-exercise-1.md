# **Ejercicio 1. [Ingress Controller / Secrets]**
Primero de todo, hará falta habilitar el addon ingress:
```
minikube addons enable ingress
```
<br><br/>

---
### **REPLICASET**
Posteriormente, se necesita la creación y ejecución de un replicaset de forma declarativa. A continuación se puede ver el que se ha definido ***(ej3-rs.yml)***:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-rs-ex3
  labels:
    app: nginx-server
spec: 
  replicas: 3
  selector:
    matchLabels: 
      app: nginx-server
  template: 
    metadata: 
      labels: 
        app: nginx-server
    spec: 
      containers:
      - name: nginx-container
        image: nginx:1.19.4
        resources: 
            limits: 
                memory: "128Mi"
                cpu: 20m
            requests: 
                memory: "128Mi"
                cpu: 20m
        ports:
        - containerPort: 80
```
Posteriormente se lanza dicho replicaset:
```
kubectl apply -f ej3-rs.yml
```
<br><br/>

---
### **SERVICE**
Después, se requiere crear el servicio ***(ej3-svc.yml)***:
```
apiVersion: v1
kind: Service
metadata:
  name: service1
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```
y lanzarlo haciendo el siguiente comando:
```
kubectl apply -f ej3-svc.yml
```
<br><br/>

---
### **SECRET**
Ahora se requiere crear dos ficheros, el secret y el certificado. Se crean mediante el comando siguiente:
```
openssl req -x509 -nodes -days 9999 -newkey rsa:2048 -keyout HOSTNAME.key -out HOSTNAME.cer -subj "/CN=miqueljorda.student.lasalle.com"
```
Con el contenido de estos dos ficheros, se genera un nuevo fichero de tipo secret especificando el certificado y la key. El contenido de esos dos campos es una sola linea, es decir, se han eliminado anteriormente todos los saltos de linea ***(ej3-secret.yml)***.

```
apiVersion: v1
kind: Secret
metadata:
  name: secret-nginx
type: kubernetes.io/tls
data:
  tls.crt: |
    MIIDNTCCAh2gAwIBAgIURHchLzP1dm/XBA18hmY023ncPC4wDQYJKoZIhvcNAQELBQAwKjEoMCYGA1UEAwwfbWlxdWVsam9yZGEuc3R1ZGVudC5sYXNhbGxlLmNvbTAeFw0yMDExMTkxMTIyNDJaFw00ODA0MDUxMTIyNDJaMCoxKDAmBgNVBAMMH21pcXVlbGpvcmRhLnN0dWRlbnQubGFzYWxsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC68IdzAUl8XVY9J0bzSNnb0xAXC4Cj/pyh9vpVFkjH2BtpHNpZMzki7eFJsGfIuOlnLBGNkQdaSPqT9TWYpKm5Qa1WnHXtc6keOGXKQdT1mEMdy/DDHC6jEUT/vgxZnXfVBE7Xa44cxcawpQdPIh+ru9OCBdUFRIkmnzVELABKkhMvf2NWZ5AHpj3XETyyI9mD1PMLswpeIcLzPqh4PAG5BZae3QMRKk2zKYgR4zOd24+1FDKf+vAfSnUKkuUbBLHMh4ZZck/dYYGYGgLQQjPXxs9WWmv6Tk60rG69umQY3kINuzNQuqZ04ycNL7MtgBtVBkVnFSNhiJMhqzdqt8qrAgMBAAGjUzBRMB0GA1UdDgQWBBT5dMe8eL97LmoMdgjDEbbbU2+oiTAfBgNVHSMEGDAWgBT5dMe8eL97LmoMdgjDEbbbU2+oiTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQCmSfXV6BTnL+WZt3IUzBkXDY/aq7TDUL/8MsSeMwV1h31pf/m5EAMkOZkoETFqt/0dyW6O/uCc6+e0gpp9DB7AN0dJ/42p/QlJlvUc056/sSiWosLvqr3ylCxFj5MbMnlVn05Rn8Hz2yorfoAdbsb7+EqSr6UZu6vhad2POtlxJndamwKg5QWaeMVKXzF1GK9w0n5ZLq+b/h0y9v1Z5j56APyugnmilMR3mVox/P1CfUEjn00h1lRbAvOE2aFv3AX8WbvT2aBdSqJUD5zHBBcOIFhhZJ8V2PuhSrGocTgSsIKCI7UCEzoPCs5xkuaM1nq4UHgZsWGtaMZ6bwNbUTMK
  tls.key: |
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC68IdzAUl8XVY9J0bzSNnb0xAXC4Cj/pyh9vpVFkjH2BtpHNpZMzki7eFJsGfIuOlnLBGNkQdaSPqT9TWYpKm5Qa1WnHXtc6keOGXKQdT1mEMdy/DDHC6jEUT/vgxZnXfVBE7Xa44cxcawpQdPIh+ru9OCBdUFRIkmnzVELABKkhMvf2NWZ5AHpj3XETyyI9mD1PMLswpeIcLzPqh4PAG5BZae3QMRKk2zKYgR4zOd24+1FDKf+vAfSnUKkuUbBLHMh4ZZck/dYYGYGgLQQjPXxs9WWmv6Tk60rG69umQY3kINuzNQuqZ04ycNL7MtgBtVBkVnFSNhiJMhqzdqt8qrAgMBAAECggEALISRTRL2ZBZ6LhAHdnJX52CpCUPk+6jrtipxzQeEh4rwZCTJ3sozBh1uNDU9UfCvLLGy8GqkPJz6h3eyrsov3P1GAUU7VDPiB19Y5kStKVt8SEFD8cLpXE4Fh33DxSeZ3PGssOJdMn7q5TA3wGGY9zXAlcs7wohivP7XGYf8/cTI3MX8Jx7TdpBTyF5/l8Okm9b15K71skiM2YGwj8BYi/RpmDLfr2OKe76oeBu33c0ELRG0eX0fwqpH61dbX//3tGxMlek7+WVEvnvhfNVJ4c6NGkyp++rZdRL7SDmwvzEGDoitYKbaFzKLADNquNMUwXgRRgevUilmDtMxJDhHUQKBgQDzyLZoIrnaUixZSMV5QRgWrI1OlBvN1OHHUIKEZDrM2AzZrgYzBPZm65Z9v7TCXdJecxSRno6NODToYVrHjU+R1L4kfgG+H1Auw5QppRB+sF5zuYHgvZT8qa0mPBegs2K7E6QslNRr0/57XxkfI8DEq076bOYrA3SNQJn0Njx4YwKBgQDETpwWdjWEJjurwfGt6qtnhKH6QphteneFXq/Wb53M1I4o0AaQPHCgiaav/n2iz6ld+H/GnT23TI1DvWxMNsQ8ngvc8OoAd9OUrOp/OE3C4bAwCFqyO9dEGgxAh/vyPrnrd+Nbz3bXV0h2bI1BJnjctv0kAJ01Urtk/vMMhwOjGQKBgAlXJF4DDpnl0hstpZtKujCMNfHPzOzq/ieT1xxZdsNId92TRFuKEiLJXf+9a6CS1a+WuzAFIjMY9zX8HO1OZcERpKNjTI/LRVg1WCTsVSB1g3jtn6NLwDyaB69SWBlpJhxC2WR341eqTLLPZvgBd65RXfpMV9lIDZwn4H4x4ck/AoGBAKX0fuhOTrfFSrTNZO9OcFE3Q2g6XHpRCjHp9mUmPxUmrHe8RZAySFSvigdG8hdtzxTuO7dAvyfyw4n83U0hD4zfX9AMqwZyRqBt/mBGzRZvjW9N4L7E8BtWmf0D+O62MJ7r332CSkmKNGxJILFvQMX2vNJu4nht1JZxIi/5BA4hAoGAJx7LSrPuDqY+PWzJfnY18MoNquo15MZ6jwMFJ2L75k4XKy6vG3GfzqaXaIzkefc5vhdjlym6vOw0GRdzqP/wnM4k/iWEOPFBxZ46BfVxgFVDjIsMq0GwDiKG6ukcvOxU8T8aZ+CeIQkwjQvw2E4dDgXDZZ85XWkFaO34Kl/LK8U=
```
Ahora, se lanza el secret:
```
kubectl apply -f ej3-secret.yml
```
<br><br/>

---
### **INGRESS CONTROLLER**
Posteriormente, se declara el ingress controller y se lanza de la misma manera que en casos anteriores ***(ej3-ic.yml)***:
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
spec:
  tls:
  - hosts:
      - https-miqueljorda.student.lasalle.com
    secretName: secret-nginx
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: service1
            port:
              number: 80
    host: miqueljorda.student.lasalle.com
```
Coger la ip del ingress controller con: 

```
describe ingress controller
```

y posteriormente añadirla en el fichero de hosts "C:\Windows\System32\drivers\etc\hosts" la ip con el valor sacado anteriormente.

<br><br/>

---
---
### **CAPTURAS**
*En la siquiente captura se puede apreciar como se pudo acceder al contenido de nginx mediante la ruta especificada:*
![](./resources/images/captura-https.PNG)