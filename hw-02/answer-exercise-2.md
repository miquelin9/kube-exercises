# **Ejercicio 2. *Crear un  objeto  de  tipo replicaSet a partir del  objeto  anterior con  las siguientes especificaciones:*** 
##  **• Tener tres réplicas**
---
*A continuación se muestra el contenido del ReplicaSet creado, llamado **replicaSet.yml**:*
```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-exercise2
  labels:
    app: nginx-server
    tier : app
spec: 
  replicas: 3
  selector:
    matchLabels: 
      app: nginx-server
  template: 
    metadata: 
      labels: 
        app: nginx-server
    spec: 
      containers:
      - name: nginx-container
        image: nginx:1.19.4
        resources: 
            limits: 
                memory: "256Mi"
                cpu: 100m
            requests: 
                memory: "256Mi"
                cpu: 100m
```
Para lanzar el objeto de tipo replica set, habrá que ejecutar el siguiente comando:
```
kubectl apply -f replicaSet.yml
```
<br><br/>
## **• ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?**
---
El comando que se usaría para escalar dicho replica set es el siguiente:
```
kubectl scale rs nginx-exercise2 --replicas=10***
```
<br><br/>
## **• Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor?**
---
Se adaptaría mucho mejor el daemon set, ya que actúa igual que el replica set 
con la principal diferencia que prioriza tener una réplica en cada pod.