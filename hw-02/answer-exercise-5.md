# **Ejercicio 5. Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1**
## **Las especificaciones que se han tenido en cuenta para BlueDeployment son las siguientes:**
---
### **BlueDeployment.yml**
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blue-1.0.0
  labels:
    app: blue-1.0.0
spec:
  replicas: 3
  selector:
    matchLabels:
      name: blue-1.0.0
      version: "1.0.0"
  template:
    metadata:
      labels:
        name: blue-1.0.0
        version: "1.0.0"
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            memory: "256Mi"
            cpu: 100m
          requests:
            memory: "256Mi"
            cpu: 100m
        ports:
        - containerPort: 80
```
### **BlueService.yml**
```
apiVersion: v1
kind: Service
metadata: 
  name: nginx
  labels: 
    name: nginx
spec:
  ports:
    - name: http
      port: 80
      targetPort: 80
  selector: 
    name: blue-1.0.0
    version: "1.0.0"
  type: LoadBalancer
```
<br><br/>

## **Las especificaciones que se han tenido en cuenta para GreenDeployment son las siguientes:**
---
### **GreenDeployment.yml**
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: green-1.1.0
  labels:
    app: green-1.1.0
spec:
  replicas: 3
  selector:
    matchLabels:
      name: green-1.1.0
      version: "1.1.0"
  template:
    metadata:
      labels:
        name: green-1.1.0
        version: "1.1.0"
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            memory: "256Mi"
            cpu: 100m
          requests:
            memory: "256Mi"
            cpu: 100m
        ports:
        - containerPort: 80
```
### **GreenService.yml**
```
apiVersion: v1
kind: Service
metadata: 
  name: nginx
  labels: 
    name: nginx
spec:
  ports:
    - name: http
      port: 80
      targetPort: 80
  selector: 
    name: green-1.1.0
    version: "1.1.0"
  type: LoadBalancer
```
<br><br/>

## **Metodología para el ejercicio**
---
Para proceder con el ejercicio, primero de todo se creó el blue deployment haciendo uso del siguiente comando:
```
kubectl apply -f BlueDeployment.yml
```

Posteriormente, se lanzó el servicio haciendo el siguiente comando:
```
kubectl apply -f BlueService.yml
```

En este punto, se puede comprobar que el servicio está funcionando haciendo:
```
minikube service nginx --url
```
y accediendo a la url que devuelve dicho comando.


Una vez hecho esto, se lanzó el green deployment con el siguiente comando: 
```
kubectl apply -f GreenDeployment.yml
```

Antes de actualizar el servicio, se puede hacer un describe del servicio para comprobar que los endpoints estan apuntando a las IPS de los pods del blue deployment:
```
kubectl describe svc nginx
```

Llegado este punto, se actualizó el servicio lanzado anteriormente con el fichero GreenService.yml, el cual es el mismo pero apunta hacia el GreenDeployment.
```
kubectl apply -f GreenService.yml
```

Pues, una vez actualizado el servicio, volvemos a ejecutar los comandos de describe y get url para corroborar que está corriendo correctamente y que está apuntando a los pods del green deployment:
```
kubectl describe svc nginx
minikube service nginx --url
```
