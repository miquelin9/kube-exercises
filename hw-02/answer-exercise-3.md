# **Ejercicio 3. *Crea un objeto  de  tipo service  para  exponer  la  aplicación  del  ejercicio anterior de las siguientes formas:***
## **• Exponiendo el servicio hacia el exterior (crea service1.yaml)**
---
A continuación se muestra el ***service1.yml***:
```
apiVersion: v1
kind: Service
metadata:
  name: service1
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

Para comprobar su funcionalidad, se deberá ejecutar el servicio y el pod (haciendo que corresponda el nombre del selector) 
```
kubectl apply -f service1.yml
kubectl apply -f pod1.yml
```
y luego deberemos hacer el siguiente comando para obtener la url:

```
minikube service service1 --url
```
Con esta url, se podrá ir al navegador y comprobar que esta funcionando correctamente.
<br><br/>
## **• De forma interna, sin acceso desde el exterior (crea service2.yaml)**
---
A continuación se muestra el ***service2.yml***:

```
apiVersion: v1
kind: Service
metadata:
  name: service2
spec:
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

Para comprobar este ejercicio, primero de todo se lanzará el servicio usando:
```
kubectl apply -f service2.yml
```
Posteriormente se usará el describe para corroborar que el servicio tiene los endpoints bien asignados:
```
kubectl describe svc service2
```

Posteriormente, habrá que conectarse a uno de los pods haciendo:

```
kubectl get pods (obtención de uno de los nombres)
kubectl exec -stdin -tty <podname> -- bin/bash
curl <CLUSTER_IP>:80
```

<br><br/>
## **• Abriendo un puerto especifico de la VM (crea service3.yaml)**
---
A continuación se muestra el ***service3.yml***:
```
apiVersion: v1
kind: Service
metadata:
  name: service3
spec:
  type: NodePort
  selector:
    app: nginx-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30010
```

De nuevo, lo primero que habrá que hacer es lanzar el servicio creado en el yaml:
```
kubectl apply -f service3.yml
```
En este caso, hay que abrir un puerto en nuestra máquina virtual. En mi caso, en VirtualBox.
Después, hay que especificar en el servicio dicho puerto en el campo nodePort añadiendo también dicho tipo.
Después, se podrá ver el contenido accediendo a la url especificada y el puerto especificado, siendo en mi caso:
```
localhost:30010
```