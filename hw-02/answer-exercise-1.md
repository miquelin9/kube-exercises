# **Ejercicio 1. *Crea un pod de forma declarativa con las siguientes especificaciones:*** 
*El fichero que se muestra a continuación es el pod usado para el primer ejercicio, llamado **pod1.yml**:*

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-exercise1
  labels:
    app: nginx-server
spec:
  containers:
  - name: nginx
    image: nginx:1.19.4-alpine
    ports:
      - containerPort: 80
        hostPort: 1234
    resources:
        limits:
            memory: "256Mi"
            cpu: 100m
        requests:
            memory: "256Mi"
            cpu: 100m
```
Para lanzar dicho pod, hay que ejecutar el siguiente comando:
```
kubectl apply -f pod1.yml
``` 
<br><br/>

## **• ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?**
---
El comando usado para obtener dichas lineas de logs es el siguiente:
```
kubectl logs -f nginx-exercise1 --tail=10
```
<br><br/>

## **• ¿Cómo podría obtenerla IP interna del pod? Aporta capturas para indicar el proceso que seguirías.**
---
El comando que se puede apreciar a continuación nos saca información ampliada del pod, dentro de la cual podemos apreciar la IP interna.
```
kubectl get pod -o wide
```
<br><br/>

## **• ¿Qué comando utilizarías para entrar dentro del pod?**
---
Para acceder dentro del pod se usa el siguiente comando:
```
kubectl exec -it nginx-exercise1 -- sh
```
<br><br/>

## **• Necesitas  visualizar  elcontenido  que  expone NGINX, ¿Qué  acciones debes llevar a cabo?**
---
Para hacer este ejercicio se ha entendido el enunciado como que se pretende acceder al contenido del fichero *index.html*.
```
kubectl exec --stdin --tty nginx-exercise1 -- /bin/bash
cd /usr/share/nginx/html
cat index.html
```
<br><br/>

## **• Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?**
---
Usando el siguiente comando podemos extraer información del pod en formato *yaml*. Dentro de esa información se podrá ver que hay una propiedad que hace referencia a dicho ***QoS***:
```
kubectl get pod nginx-exercise1 --output=yaml
```