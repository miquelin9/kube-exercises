# **Ejercicio 4. Crear un objeto de tipo *deployment* con las especificaciones del ejercicio 1**
## **• Despliega  una  nueva  versión  de  tu  nuevo  servicio  mediante  la  técnica “recreate”**
---

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  strategy: 
    type: Recreate
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v2.0.0
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            memory: "256Mi"
            cpu: 100m
          requests:
            memory: "256Mi"
            cpu: 100m
        ports:
        - containerPort: 80
          hostPort: 1234
```
<br><br/>
## **• Despliega una nueva versión haciendo “rollout deployment”**
---

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
        version: v2.0.0
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.4-alpine
        resources:
          limits:
            memory: "256Mi"
            cpu: 100m
          requests:
            memory: "256Mi"
            cpu: 100m
        ports:
        - containerPort: 80
          hostPort: 1234
```
<br><br/>
## **• Realiza un rollback a la versión generada previamente**
---
Uso el comando a continuación para ver las ID de las diferentes versiones:
```
kubectl rollout history deployment nginx-deployment
```
El comando usado para hacer un rollback es el siguiente
```
kubectl rollout undo deployment nginx-deployment --to-revision=1
```
